var searchData=
[
  ['deepsom_12',['DeepSOM',['../class_deep_s_o_m.html',1,'DeepSOM'],['../class_s_o_m.html#a1c4bed0ccb594c7260162aa859a31013',1,'SOM::DeepSOM()'],['../class_deep_s_o_m.html#a93c3fcba005e6158d9a3054fc836131f',1,'DeepSOM::DeepSOM(int t_lim)'],['../class_deep_s_o_m.html#a858a74d08dfedd8fcdd4e6cdd936dd33',1,'DeepSOM::DeepSOM(DeepSOM &amp;&amp;)=delete']]],
  ['deepsom_2ecpp_13',['DeepSOM.cpp',['../_deep_s_o_m_8cpp.html',1,'']]],
  ['deepsom_2ehpp_14',['DeepSOM.hpp',['../_deep_s_o_m_8hpp.html',1,'']]],
  ['distance_5fsqr_15',['distance_sqr',['../class_l_v_q.html#ad3acd952607e912cb50e8cfca0950f41',1,'LVQ::distance_sqr()'],['../class_s_o_m.html#a17a7c934a0befdd6ab629bb90773f762',1,'SOM::distance_sqr()']]]
];
