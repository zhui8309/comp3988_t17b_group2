var searchData=
[
  ['batch_5fblock_5',['batch_block',['../struct_deep_s_o_m_1_1_node.html#a32935d3175eb09165fbce78e1171277c',1,'DeepSOM::Node']]],
  ['batch_5fiter_6',['batch_iter',['../struct_deep_s_o_m_1_1_node.html#ac4d9a9ac29970327c28bb433b13de8e5',1,'DeepSOM::Node']]],
  ['batch_5ftrain_7',['batch_train',['../class_deep_s_o_m.html#aacd36c0335d0bd388281ea4aee8cbfe6',1,'DeepSOM::batch_train()'],['../class_s_o_m.html#a10de9d7ff05633e724cf8d440792b7b2',1,'SOM::batch_train()']]],
  ['batch_5ftrain_5fblock_8',['batch_train_block',['../class_deep_s_o_m.html#a2468836805edc5c887bedd51c19c3298',1,'DeepSOM']]],
  ['batch_5ftrain_5fiter_9',['batch_train_iter',['../class_s_o_m.html#a91bd9eb3edd2073b563d25a83009930d',1,'SOM']]]
];
